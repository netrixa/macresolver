﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using CuttingEdge.Conditions;

namespace MacResolver
{
    internal static class Program
    {
        [DllImport("Iphlpapi.dll", EntryPoint = "SendARP")]
        private static extern int SendArp(int destinationIp, int sourceIp, byte[] macAddressBytes, ref int length);

        private static void Main()
        {
            Console.Write("Please enter the hostname: ");

            var host = Convert.ToString(Console.ReadLine()).ToLower();
            var hostAddress = Dns.GetHostAddresses(host).First(address => address.AddressFamily == AddressFamily.InterNetwork);

            if (string.IsNullOrEmpty(host))
                host = Dns.GetHostName().ToLower();

            var macAddress = GetMacAddress(hostAddress);

            Console.WriteLine("The MAC address for host {0} is {1}.", host, macAddress.ToString("x2"));
            Console.ReadLine();
        }

        private static PhysicalAddress GetMacAddress(IPAddress ipAddress)
        {
            Condition.Requires(ipAddress).IsNotNull();
            
            try
            {
                var destinationIp = BitConverter.ToInt32(ipAddress.GetAddressBytes(), 0);
                const int sourceIp = 0;
                var length = 6;
                var macAddressBytes = new byte[length];
                var result = SendArp(destinationIp, sourceIp, macAddressBytes, ref length);

                if (result != 0)
                    throw new Exception(string.Format("SendARP failed with error code {0}.", result));

                return new PhysicalAddress(macAddressBytes);
            }
            catch (Exception exception)
            {
                throw new Exception(string.Format("The MAC address for the host {0} could not be obtained.", ipAddress), exception);
            }
        }

        private static string ToString(this PhysicalAddress physicalAddress, string format)
        {
            Condition.Requires(physicalAddress).IsNotNull();
            
            var addressBytes = physicalAddress.GetAddressBytes();
            var addressStringBuilder = new StringBuilder();

            for (var index = 0; index < addressBytes.Length; index++)
            {
                var addressByte = addressBytes[index];

                addressStringBuilder.Append(addressByte.ToString(format));

                if (index != addressBytes.Length - 1)
                    addressStringBuilder.Append("-");
            }

            return addressStringBuilder.ToString();
        }
    }
}